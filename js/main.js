$(document).ready(function() {

	$(".menuPrincipal ul li").hover(function() {
		$(this).find('ul').stop().show();
		$(this).find('a').addClass('acti');
	}, function() {
		$(this).find('ul').stop().hide();
		$(this).find('a').removeClass('acti');
	});
	


	$("#videoLink").find('a').each(function(){
		//valida extencion
		var linkHref = $(this).attr('href');
		var extencion = linkHref.substring(linkHref.lastIndexOf("."));
		if (extencion !=".mp4") {
			console.log("otra extencion");
		}else{
			$(this).attr('linkVideo', linkHref);
			$(this).attr('target', '');
			$(this).attr('href', 'javascript:;');
		}
	});
	$("#videoLink a").click(function(event) {
		var linkDirecto =($(this).attr('linkVideo'));
		$(".contVideoAct ").find('video').attr('src', linkDirecto);
		$(".modalVideo").fadeIn();
	});
	$(".contVideoAct i").click(function(event) {
		$(".modalVideo").fadeOut();
		$(".contVideoAct ").find('video').attr('src', "");
	});

	
	$(".contMenuLateral ul li a").click(function(event) {
		$(this).parent().addClass('visi');
		$(this).parent().find('ul').stop().slideToggle();
	});

	$(".Contendor a").fancybox({});


	Tipped.create('.box');

	$('#MenuPrin a').click(function(e){		
		$('.contMenuLateral a').removeClass('acti');
		var strAncla=$(this).attr('href'); //id del ancla
		$('html, body').animate({ scrollTop:$(strAncla).offset().top-(90)}, 1000);
		$(this).addClass('acti');
    	e.preventDefault();
	});
	

	$(".MenuClon").on( "click", "#MenuPrin a", function() {
	  	$('#MenuPrin a').removeClass('acti');
		var strAncla=$(this).attr('href'); //id del ancla
		$('html, body').animate({ scrollTop:$(strAncla).offset().top-(90)}, 1000);
		$(this).addClass('acti');
	});

	$("#MenuPrinTab a").click(function() {
		var strTab=$(this).attr('href');
	});
	//TabMenu Preguntas
	$("#MenuPrinTab a").click(function() {
		$("#MenuPrinTab a").removeClass("acti");
		$(this).addClass("acti");
		$(".contPreguntasItem").hide();
		var activeTab = $(this).attr("href");
		$(activeTab).fadeIn();
		return false;
	});

	$("#MenuPrinTabPreguntas a").click(function() {
		$("#MenuPrinTabPreguntas a").removeClass("acti");
		$(this).addClass("acti");
		$(".contPreguntasItem").hide();
		var activeTab = $(this).attr("href");
		$(activeTab).fadeIn();
		return false;
	});

	var URLactual = location.hash;
    var implemetacion = "#implementacion";
    var postVenta = "#postVenta";
    var Venta = "#herramienta_venta";
    var pagos = "#pago-tarjeta";

    console.log(URLactual);

    if(URLactual == implemetacion ){
    	$("#MenuPrinTab a").removeClass("acti");
		$("#MenuPrinTab").find("li:nth-child(2) a").addClass("acti");
		$(".contPreguntasItem").hide();
		$("#implementacion").fadeIn();
		return false;
    }else if(URLactual == postVenta ){
    	$("#MenuPrinTab a").removeClass("acti");
		$("#MenuPrinTab").find("li:nth-child(3) a").addClass("acti");
		$(".contPreguntasItem").hide();
		$("#postVenta").fadeIn();
    }else if(URLactual == Venta ){
    	$("#MenuPrinTab a").removeClass("acti");
		$("#MenuPrinTab").find("li:nth-child(1) a").addClass("acti");
		$(".contPreguntasItem").hide();
		$("#herramienta_venta").fadeIn(); 
    }else if(URLactual == pagos){
    	$('.contMenuLateral a').removeClass('acti');
		$('html, body').animate({ scrollTop:$(URLactual).offset().top-(70)}, 1000);
		//$(this).addClass('acti');
    	e.preventDefault();
    }


	//$(".contSidebar").clone().prependTo('.MenuClon');
	$(window).scroll(function () {
		 var lista = $(this).scrollTop();
		 var valor = lista/100;
		 //console.log(valor)
		 
		 //if(valor>=5.5){
		 	//$(".MenuClon").fadeIn();
		 //}else{
		 	//$(".MenuClon").hide();
		 //}
	     $('.contLogoResp img').css({ transform: 'rotate(' + valor + 'rad)' });
	    if (valor >= 2) {
				$(".contmenuResponsivo").stop().fadeIn();
		}else{
			$(".contmenuResponsivo").stop().fadeOut();
		}

	});
});